package experiments;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class AvailabilityReport
{
    public static void main(String[] args) throws IOException, ParserConfigurationException, TransformerException
    {
        System.out.println("Prosím, zadejte maximální počet pokusů o připojení:");

        Scanner scanner = new Scanner(System.in).useDelimiter("[ ,\n|;]");

        int count = scanner.nextInt();

        System.out.println("Prosím, zadejte webové adresy:");

        ArrayList<String> urls = new ArrayList<>();
        while (true)
        {
            String url = scanner.next();
            if (url.equals("END"))
            {
                break;
            }
            urls.add(url);
        }

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

        Document document = documentBuilder.newDocument();
        Element htmlElement = document.createElement("html");
        document.appendChild(htmlElement);
        htmlElement.setAttribute("xmlns", "http://www.w3.org/1999/xhtml");
        Element bodyElement = document.createElement("body");
        htmlElement.appendChild(bodyElement);
        Element ulElement = document.createElement("ul");
        bodyElement.appendChild(ulElement);

        for (String url : urls)
        {
            boolean ok = false;
            for (int attempt = 0; attempt <= count; attempt++)
            {
                int respCode;
                try
                {
                    respCode = Utils.GetResponseCodeFromURL(url);
                    ok = respCode == 200;
                } catch (UnknownHostException e)
                {
                    ok = false;
                }
                if (ok)
                {
                    break;
                }
            }

            Element liElement = document.createElement("li");
            ulElement.appendChild(liElement);
            Element spanElement = document.createElement("span");
            liElement.appendChild(spanElement);
            if (ok)
            {
                spanElement.setTextContent(url + ": Available");
            } else
            {
                spanElement.setTextContent(url + ": Unavailable");
            }
        }

        Path path = Paths.get(System.getProperty("user.dir"), "report.xhtml");
        Utils.writeXml(document, new FileOutputStream(path.toString()));

        // TODO

        System.out.println("Hotovo. Report najdete v následujícím souboru:");
    }
}
